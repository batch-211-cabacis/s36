const express = require ("express");
const router = express.Router();

const taskControllers = require ("../controllers/taskControllers");

// Routes are responsible for defining the URIs that our client accesses and the coreesponding controller functions that will be used when routes is accessed

// Route to get all the tasks
// this route expects to receive  a get request at the URL "/tasks"
// the whole URL is at "http://localhost:3001/tasks"

router.get("/", (req,res) => {
	taskControllers.getAllTasks().then(resultFromController=>res.send(resultFromController))
})


router.post("/",(req,res)=> {
	taskControllers.createTask(req.body).then(resultFromController=>res.send(
		resultFromController))
})

// Route to delete a task
// expects to receive a Delete request at the URL "tasks/:id"
// the whole URL is http://localhost:3001/tasks/:id
router.delete("/:id",(req,res)=>{
	taskControllers.deleteTask(req.params.id).then(resultFromController=>res.send(resultFromController))
})


router.put("/:id",(req,res)=> {
	taskControllers.updateTask(req.params.id,req.body).then(resultFromController=>res.send(resultFromController))
})



// ACTIVITY

router.get("/:id", (req,res) => {
	taskControllers.getTask(req.params.id).then(resultFromController=>res.send(resultFromController))
})


router.put("/:id/complete",(req,res)=>{
	taskControllers.updateTask(req.params.status).then(resultFromController=>res.send(resultFromController))
})



// use "module.exports" to export the router object to use in the index.js
module.exports = router;